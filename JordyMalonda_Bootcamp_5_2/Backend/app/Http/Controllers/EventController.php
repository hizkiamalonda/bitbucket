<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\event;
use App\ticket;
use Illuminate\Support\Facades\DB;

class EventController extends Controller
{
	function GetEvent()
    {
    	$event = event::get();				

    	return response()->json($event, 200);	
    }

    function GetTicket()
    {
        $ticket = ticket::get();              

        return response()->json($ticket, 200);   
    }

    function AddEvent(Request $request)				
    {
    	
    	DB::beginTransaction();						

    	try
    	{
    		$this->validate($request, 										//  validasi data-data penting.
    			[
                'name' => 'required',
                'location' => 'required',
                
                ]);

    		if(isset ($request->id))              
            {
    

            $data = event::find($request->id);
            $data->name = $request->input('name');               
            $data->location = $request->input('location');
            $data->date = $request->input('date');
            $data->save();    

            $event = event::find($request->id);


            }
            else
            {

        		$name = $request->input('name');				
        		$location = $request->input('location');
        		$date = $request->input('date');

        		

        		$data = new event;
        		$data->name = $name;
        		$data->location = $location;
        		$data->date = $date;
        		$data->save();

                $event = event::orderBy('id' , 'desc')->first();                                        
            }
                

        		DB::commit();

        		return response()->json($event, 200);				// object dalam bentuk json dengan status code 200
        	
        }	
    
    	catch(\Exception $e)
    	{
    		DB::rollback();
    		return response()->json(["message" => $e->getMessage()], 500);			// untuk verikasi gagal
    	}
    }

     function removeTicket(Request $request)
    {
        DB::beginTransaction();

        try
        {
            $this->validate($request,                                       
                ['id' => 'required']);


            $id = (integer)$request->input('id');
            $data = ticket::find($id);
            if ($data->qty > 0 ) {
                $data->qty = $data->qty - 1;  
            }
            $data->save();
            $ticket = ticket::get();

            DB::commit();

            return response()->json($ticket, 200);
        }
        
        catch(\Exception $e)
        {
            DB::rollback();
            return response()->json(["message" => $e->getMessage()], 500);
        }




    }

}
