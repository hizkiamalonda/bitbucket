<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});




Route::group(['prefix' => 'event'] , function()			
{
	Route::get('/' , 'EventController@GetEvent');		
	Route::post('/add' , 'EventController@AddEvent');				
	
});

Route::group(['prefix' => 'ticket'] , function()			
{
	Route::get('/' , 'EventController@GetTicket');		
	Route::post('/remove' , 'EventController@removeTicket');				
	
});