import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';                 // mengaktifkan Http Request nya
import { RouterModule, Routes } from '@angular/router';     // mengaktifkan RouterLink nya

import { EventService } from './event.service'

import { AppComponent } from './app.component';
import { EventMasterComponent } from './event-master/event-master.component';
import { EventListComponent } from './event-list/event-list.component';

@NgModule({
  declarations: [
    AppComponent,
    EventMasterComponent,
    EventListComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot([                                  // Daftarin apa saja yg akan di routerLink
    { path: '' , component: EventListComponent },
    { path: 'addEvent' , component: EventMasterComponent },
    
    ])
  ],
  providers: [EventService],
  bootstrap: [AppComponent]
})
export class AppModule { }
