import { Component, OnInit } from '@angular/core';
import { EventService } from '../event.service';

@Component({
  selector: 'app-event-list',
  templateUrl: './event-list.component.html',
  styleUrls: ['./event-list.component.css']
})
export class EventListComponent implements OnInit {

  constructor(private api:EventService) { }


  event:object[];
  ticket:object[];


  ngOnInit() {
 		
  		this.api.getEvent().subscribe(result => this.event = result);
  		this.api.getTicket().subscribe(result => this.ticket = result);

  }


  removeTicket(id:number){
    this.api.removeTicket(id)
            .subscribe(result => this.ticket = result);
            console.log(id);
  }


}
