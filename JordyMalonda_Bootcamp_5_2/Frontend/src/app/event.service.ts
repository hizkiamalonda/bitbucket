import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Http, Headers , RequestOptions } from '@angular/http';                 // mengaktifkan Http Request nya
import { Observable } from 'rxjs/Rx';											                      // kombinasi telor keju
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';		

@Injectable()
export class EventService {

  constructor(private http:Http , private router:Router ) { }


    getEvent(){
  	
  	let headers = new Headers({ "Content-type" : "application/json"});
  	let options = new RequestOptions({ headers : headers });

  			return this.http.get('http://localhost:8000/api/event', options)
  			 .map(result => result.json());
  }




  addEvent(name:string , location:string , date:string) { 
  	
  	let event = {
  		"name" : name,
  		"location" : location,
  		"date" : date
  	}

  	let body = JSON.stringify(event);
  	let headers = new Headers({ "Content-type" : "application/json" });
  	let options = new RequestOptions({ headers : headers });

  	return this.http.post('http://localhost:8000/api/event/add', body, options)
    .map(result => result.json());
  	// .subscribe(
  	// 	result => {}
  	// 		localStorage.setItem('token',result.json().token);
  	// 		this.router.navigate(['/user']);
  	// 	},
  	// 	err => {
  	// 		localStorage.removeItem('token');
  	// 		this.router.navigate(['/register']);
  	// 	}
  	// 	);
  }


    getTicket(){
  	
  	let headers = new Headers({ "Content-type" : "application/json"});
  	let options = new RequestOptions({ headers : headers });

  			return this.http.get('http://localhost:8000/api/ticket', options)
  			 .map(result => result.json());
  }

    removeTicket(id:number){
    
    let data = {
      "id" : id
    }

    let body = JSON.stringify(data);
    let headers = new Headers({ "Content-type" : "application/json" });
    let options = new RequestOptions({ headers : headers });

    return this.http.post('http://localhost:8000/api/ticket/remove', body, options)
    .map(result => result.json());
  }

}
