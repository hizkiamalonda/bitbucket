import { Component, OnInit } from '@angular/core';
import { EventService } from '../event.service';

@Component({
  selector: 'app-event-master',
  templateUrl: './event-master.component.html',
  styleUrls: ['./event-master.component.css']
})
export class EventMasterComponent implements OnInit {

  constructor(private api:EventService) { }


  event:object[];


  ngOnInit() {
  		
  }


  name:string="";
  location:string="";
  date:string="";

  addEvent(){
  	this.api.addEvent(this.name, this.location , this.date)
            .subscribe(result => this.event = result);

    this.name = "";
    this.location = "";
    this.date = "";


  } 

}
