<?php


namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\transaction;						// untuk memakai model nya
use Illuminate\Support\Facades\DB;		// untuk memakai metode eloquent ORM

class BookingController extends Controller
{
    function InputData(Request $request)				// request untuk ngambil data untuk disimpan di database
    {
    	// membungkus insert DB sampai commit, kalau salah satu gagal lgsg di rollback dalam catch.
    	DB::beginTransaction();						

    	try
    	{
    		

    		// ngambil data yg dikirim dari client dan 'name' adalah key dari data json yg dimasukin client.

    		
            $customer = $request->input('customer_id');
            $room = $request->input('room_id');
            $in = $request->input('check_in');
            

    		// untuk nge save ke database

    		$data = new transaction;
    		$data->customer_id = $customer;
    		$data->room_id = $room;
    		$data->check_in = $in;

    		$data->save();                                        // // untuk menyimpan data pada database

            $get = transaction::get();

    		DB::commit();

    		return response()->json($get, 200);				// untuk verifikasi berhasil
    	}
    	
    	catch(\Exception $e)
    	{
    		DB::rollback();
    		return response()->json(["message" => $e->getMessage()], 500);			// untuk verikasi gagal
    	}
    }
}
