import { LaravelPage } from './app.po';

describe('laravel App', () => {
  let page: LaravelPage;

  beforeEach(() => {
    page = new LaravelPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
