import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-room-list',
  templateUrl: './room-list.component.html',
  styleUrls: ['./room-list.component.css']
})
export class RoomListComponent  {

roomList : Object[] = 
[ 
	{ "id":"1","room_number":"222","type":"Reguler","price":"500000","status":"Available",},
	{ "id":"1","room_number":"223","type":"Deluxe","price":"800000","status":"Available",},
	{ "id":"1","room_number":"224","type":"Luxury","price":"1200000","status":"Available",},
	{ "id":"1","room_number":"225","type":"VVIP","price":"1800000","status":"Available",},
	

]

}
