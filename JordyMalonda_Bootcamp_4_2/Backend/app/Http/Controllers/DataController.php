<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\student;
use Illuminate\Support\Facades\DB;

class DataController extends Controller
{
    function AddStudent(Request $request)				
    {
    	
    	DB::beginTransaction();						

    	try
    	{
    		$this->validate($request, 										//  validasi data-data penting.
    			[
                'first_name' => 'required',
                'last_name' => 'required',
                'email' => 'required',
                ]);

    		if(isset ($request->id))              
            {
    

            $data = student::find($request->id);
            $data->first_name = $request->input('first_name');               
            $data->last_name = $request->input('last_name');
            $data->email = $request->input('email');
            $data->phone_number = $request->input('phone_number');
            $data->course_id = $request->input('course_id');
            $data->save();    

            $student = student::find($request->id);                                    

            }
            else
            {

        		$first = $request->input('first_name');				
        		$last = $request->input('last_name');
        		$email = $request->input('email');
        		$phone = $request->input('phone_number');
        		$course = $request->input('course_id');

        		

        		$data = new student;
        		$data->first_name = $first;
        		$data->last_name = $last;
        		$data->email = $email;
        		$data->phone_number = $phone;
        		$data->course_id = $course;
        		$data->save();

                $student = student::orderBy('id' , 'desc')->first();                                        
            }
                

        		DB::commit();

        		return response()->json($student, 201);				// object dalam bentuk json dengan status code 201
        	
        }	
    
    	catch(\Exception $e)
    	{
    		DB::rollback();
    		return response()->json(["message" => $e->getMessage()], 500);			// untuk verikasi gagal
    	}
    }



    
}
