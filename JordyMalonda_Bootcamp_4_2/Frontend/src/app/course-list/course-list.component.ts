import { Component, OnInit } from '@angular/core';
import { DataServiceService } from '../data-service.service';
import {NgForm} from '@angular/forms';

@Component({
  selector: 'app-course-list',
  templateUrl: './course-list.component.html',
  styleUrls: ['./course-list.component.css']
})
export class CourseListComponent implements OnInit {

  id: string = '';
  course_name: string;
  lecture_name:string;
  price: number;

  CourseList : object[];
	
  constructor(private data:DataServiceService) { }

  ngOnInit() {
  	this.CourseList = this.data.getCourseList();
  }

  addCourse(){
    this.data.AddCourseList({
      "id":this.id,
      "course_name":this.course_name,
      "lecture_name":this.lecture_name,
      "price":this.price,
    });
  }

}
