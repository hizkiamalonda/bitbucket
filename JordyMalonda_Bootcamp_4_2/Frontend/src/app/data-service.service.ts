import { Injectable } from '@angular/core';

@Injectable()
export class DataServiceService {

  constructor() { }

private CourseList : Object[] = 
[
	{ "id":"1","course_name":"job Connector","lecture_name":"hasbi","price":"15000000",},
	{ "id":"2","course_name":"IoT","lecture_name":"Lintang","price":"10000000",},
	{ "id":"3","course_name":"Fast Track","lecture_name":"Purwa","price":"20000000",},
	{ "id":"4","course_name":"Android Development","lecture_name":"Magga","price":"8000000",},	
]

getCourseList():object[]
  {
    return this.CourseList;
  }


AddCourseList(obj:object):object[]
  {
    this.CourseList.push(obj);
    return this.CourseList;
  }
}
